﻿using CV.Interfaces;
using System;
using UnityEngine;

namespace CV.UI
{
    public class Window : MonoBehaviour, IWindow
    {
        private IWindowFactory UIManager => Root.Root.IWindowFactory;

        public bool IsVisible { get { return this.gameObject.activeSelf; } set { this.gameObject.SetActive(value); } }

        public event EventHandler Hided = (s, e) => { };
        public event EventHandler Showed = (s, e) => { };

        public virtual void Hide()
        {
            IsVisible = false;
            Hided(this, EventArgs.Empty);
            UIManager.WindowHided(this);
        }

        public virtual void Show()
        {
            IsVisible = true;
            Showed(this, EventArgs.Empty);
            UIManager.WindowShowed(this);
        }

        public string GetName()
        {
            return this != null ? this.gameObject.name : "";
        }

        public void SetName(string newName)
        {
            gameObject.name = this != null ? newName : "";
        }

        public virtual void Close()
        {
            UIManager.WindowHided(this);
            Destroy(this.gameObject);
        }

        public GameObject GetObject()
        {
            return this.gameObject;
        }
    }
}
