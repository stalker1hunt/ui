﻿using Redpill.SlotMachine;
using CV.UI;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace CV.Interfaces
{
    public interface IWindowFactory
    {
        event EventHandler OnShow;
        event EventHandler OnHide;

        Transform GetGameRoot();
        Canvas GetMainCanvas();
        void AddActiveWindow(IWindow windowView);
        bool CloseWindow(string name);
        bool IsVisibleWindow(string name);
        IWindow GetWindow(string name);
        T GetWindow<T>() where T : UnityEngine.Object, IWindow;
        bool HideWindow(string name);
        T InitializeTooltip<T>(T original, Transform transform) where T: TooltipObject;
        void CloseTooltip();
        T InitializeWindow<T>(GameObject original, Transform transform = null) where T : UnityEngine.Object, IWindow;
        bool ShowWindow(string name);
        void InitErrorWindow(string description = null, ReportType reportType = ReportType.Other, bool force = false);
        void InitInfoWindow(string description = null);
        List<IWindow> GetActiveWindows();
        void WindowShowed(IWindow window);
        void WindowClosed(IWindow window);
        void WindowHided(IWindow window);
        void WindowDestroyed(IWindow window);
        void InitBugWindow(string log = null, ReportType reportType = ReportType.Bug);

        T InitializePopupWindow<T>(GameObject popup, Transform canvas = null) where T : UnityEngine.Object, IWindow;
        void ClosePopup();
    }
}