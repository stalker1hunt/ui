﻿using System;

namespace CV.Interface
{
    public interface IWindow
    {
        event EventHandler Showed;
        event EventHandler Hided;
        bool IsVisible { get; set; }

        void Hide();
        void Show();
        void Close();
        string GetName();
        void SetName(string newName);
        UnityEngine.GameObject GetObject();
    }
}
