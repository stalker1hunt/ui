﻿using Redpill.Interfaces;
using Redpill.SlotMachine;
using Redpill.UI.Tooltips;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Redpill.UI
{
    public class WindowManager : MonoBehaviour, IUIManager
    {
        const string NAME_MAIN_CANVAS = "Canvas";

        private List<IWindow> windowViews = new List<IWindow>();
        [SerializeField] private List<GameObject> windowObjects = new List<GameObject>();
        [SerializeField] private ErrorMessageWindow errorMessageWindow;
        [SerializeField] private ErrorMessageWindow infoMessageWindow;
        [SerializeField] private ReportBugWindow bugWindow;

        private Canvas mainCanvas;
        private Transform gameRoot;

        private TooltipObject lastTooltip = null;

        public event EventHandler OnShow = (s, e) => { };
        public event EventHandler OnHide = (s, e) => { };

        private Queue<IWindow> popups = new Queue<IWindow>();
        private IUserProfile userProfile => Root.Root.UserProfile;

        public void Awake()
        {
            foreach (var _windowsObject in windowObjects)
            {
                windowViews.Add(_windowsObject.GetComponent<IWindow>());
            }
        }

#if UNITY_ANDROID
        private void Update()
        {
            if (lastTooltip != null)
            {
                if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
                    CloseTooltip();

#if UNITY_ANDROID && UNITY_EDITOR
                if (Input.GetKeyDown(KeyCode.Mouse0))
                    CloseTooltip();
#endif
            }
        }
#endif

        public T InitializeTooltip<T>(T original, Transform transform) where T : TooltipObject
        {
            if (lastTooltip != null) CloseTooltip();

            lastTooltip = Instantiate(original, transform, false);

            return (T)lastTooltip;
        }

        public void CloseTooltip()
        {
            if (lastTooltip != null) Destroy(lastTooltip.gameObject);

            lastTooltip = null;
        }

        public T InitializeWindow<T>(GameObject original, Transform transform = null) where T : UnityEngine.Object, IWindow
        {
            transform = transform ?? GetMainCanvas().transform;

            var window = windowViews.Find(x => x.GetName() == original.name) ?? Instantiate(original, transform).GetComponent<IWindow>();
            window.SetName(original.name);

            AddViewWindow(window);

            WindowShowed(window);

            return (T)window;
        }

        public void AddActiveWindow(IWindow windowView)
        {
            windowViews.Add(windowView);
        }

        public bool CloseWindow(string name)
        {
            IWindow window = GetWindow(name);

            if (window == null)
                return false;

            windowViews.Remove(window);
            windowObjects.Remove(window.GetObject());

            window.Close();
            return true;
        }

        public bool IsVisibleWindow(string name)
        {
            IWindow window = GetWindow(name);

            if (window == null)
                return false;

            return window.IsVisible;
        }

        public bool ShowWindow(string name)
        {
            IWindow window = GetWindow(name);

            if (window == null)
                return false;

            if (window.IsVisible == true)
                return true;

            window.Show();

            return true;
        }

        public bool HideWindow(string name)
        {
            IWindow window = GetWindow(name);

            if (window == null)
                return false;

            if (window.IsVisible == true)
            {
                window.Hide();

                return true;
            }

            return false;
        }

        public IWindow GetWindow(string name)
        {
            return windowViews.Find(x => x.GetName() == name);
        }

        public T GetWindow<T>() where T: UnityEngine.Object, IWindow
        {
            return windowViews.Find(x => x.GetType() == typeof(T))?.GetObject().GetComponent<T>();
        }

        public Canvas GetMainCanvas()
        {
            return mainCanvas = mainCanvas ?? GameObject.Find(NAME_MAIN_CANVAS).GetComponent<Canvas>();
        }

        public void InitErrorWindow(string description = null, ReportType reportType = ReportType.Other, bool force = false)
        {
            if (force || userProfile.UserStatisticsModel.debug.errorMessage)
            {
                var errorWindow = InitializeWindow<ErrorMessageWindow>(errorMessageWindow.gameObject);
                errorWindow.Setup(description, reportType);
            }
        }

        public void InitInfoWindow(string description = null)
        {
            var infoWindow = InitializeWindow<ErrorMessageWindow>(infoMessageWindow.gameObject);
            infoWindow.Setup(description);
        }

        public void InitBugWindow(string log = null, ReportType reportType = ReportType.Bug)
        {
            var bug = InitializeWindow<ReportBugWindow>(bugWindow.gameObject);
            bug.Setup(log, reportType);
        }

        public List<IWindow> GetActiveWindows()
        {
            var result = new List<IWindow>();
            foreach (var windowObject in windowObjects)
            {
                IWindow window = windowObject.GetComponent<IWindow>();
                if (window != null && window.IsVisible)
                    result.Add(window);
            }
            return result;
        }
        public void WindowShowed(IWindow window)
        {
            OnShow(window, EventArgs.Empty);
        }
        public void WindowClosed(IWindow window)
        {
            OnHide(window, EventArgs.Empty);
        }
        public void WindowHided(IWindow window)
        {
            OnHide(window, EventArgs.Empty);
        }
        public void WindowDestroyed(IWindow window)
        {
            OnHide(window, EventArgs.Empty);
        }

     
        public T InitializePopupWindow<T>(GameObject popupPrefab, Transform canvas = null) where T : UnityEngine.Object, IWindow
        {
            canvas = canvas ?? GetMainCanvas().transform;

            var popup = Instantiate(popupPrefab, canvas).GetComponent<IWindow>();
            popup.SetName($"{popupPrefab.name}_{popups.Count}");
            popup.GetObject().SetActive(false);

            popups.Enqueue(popup);

            if (popups.Count == 1) ShowPopupWindow();

            return (T)popup;
        }

        public void ClosePopup()
        {
            var closePopup = popups.Dequeue();

            Destroy(closePopup.GetObject());

            ShowPopupWindow();
        }

        private void ShowPopupWindow()
        {
            if (popups.Count == 0) return;

            popups.Peek().GetObject().SetActive(true);
            popups.Peek().Show();
        }

        private void AddViewWindow(IWindow window)
        {
            windowObjects.Add(window.GetObject());
            windowViews.Add(window);
        }
    }
}
